package main

import (
	"flag"
	"log"
	"net/rpc"
)

type Args struct {
	T      int //  type
	Angles map[int]byte
	Speed  int
}

var ip = flag.String("ip", "127.0.0.1", "set the ip address of server to connect")

func main() {
	flag.Parse()
	client, err := rpc.DialHTTP("tcp", *ip+":7999")
	if err != nil {
		log.Fatal("Dialing:", err)
	}

	angles := make(map[int]byte)
	angles[1] = 80
	angles[2] = 80
	angles[3] = 80
	angles[4] = 80
	angles[5] = 80
	angles[6] = 80
	angles[17] = 100

	args := &Args{T: 1, Angles: angles, Speed: 20}
	var retval int = -1
	err = client.Call("RobotRPC.Cmd", args, &retval)
	if err != nil {
		log.Fatal("rpc error: ", err)
	}
}
