package main

import (
	"flag"
	. "goexamples/CmdServer"
)

var ip = flag.String("ip", "127.0.0.1", "set the ip address of server to connect")

func main() {
	flag.Parse()

	cmdParser := CommandParser{}
	cmdParser.SetAddress(ip)
	cmdParser.Listen()
}
