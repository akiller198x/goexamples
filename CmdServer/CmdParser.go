package CmdServer

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net"
	"net/rpc"
)

const (
	ROTATE        = "req_single_move"
	PLAY          = "req_play"
	STOP          = "req_stop"
	RESET         = "req_reset"
	QUERYINFO     = "req_info"
	BASEACTION    = "req_base_action"
	CAMERAOPEN    = "req_open_camera"
	CAMERACLOSE   = "req_close_camera"
	HEART         = "req_heart"
	NEWPACKAGE    = "req_new_package"
	REMOVEPACKAGE = "req_remove"

	SIMULATE = false
)

var ipaddress string = "127.0.0.1"

type WxsCmdJson struct {
	Version string          `json:"version"`
	Src_id  string          `json:"src_id"`
	Dst_id  string          `json:"dst_id"`
	My_type string          `json:"type"`
	Content json.RawMessage `json:"content"`
}

type WxsCommand interface {
	DealWith(net.Conn, *WxsCmdJson) error
}

type CommandParser struct{}

func (cmd *CommandParser) SetAddress(ip *string) {
	ipaddress = *ip
}

func (cmd *CommandParser) Listen() error {
	tcp_addr, err := net.ResolveTCPAddr("tcp", ":9091")
	if err != nil {
		log.Fatal(err)
	}

	conn, err := net.ListenTCP("tcp", tcp_addr)
	defer conn.Close()

	for {
		conn, err := conn.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go CommandHandle(conn)
	}

	return nil
}

func CommandHandle(conn net.Conn) {
	defer conn.Close()

	var BufLength int = 256

	data := make([]byte, 0)

	buf := make([]byte, BufLength)

	for {
		n, err := conn.Read(buf)
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
		data = append(data, buf[:n]...)
		if n != BufLength {
			break
		}
	}

	content := string(data)
	log.Println("recv: ", content)
	if len(content) == 0 {
		log.Println("close")
	} else {
		wxsCmdJson := &WxsCmdJson{}
		err := json.Unmarshal(data, wxsCmdJson)
		if err != nil {
			log.Printf("new command un json err: %v", err)
			return
		}
		if cmd, err := NewCommand(wxsCmdJson); err == nil && cmd != nil {
			err = cmd.DealWith(conn, wxsCmdJson)
			if err != nil {
				log.Fatal(err)
			}

		}
	}
}

/* --------------------------- commands ------------------------------- */
func NewCommand(data *WxsCmdJson) (WxsCommand, error) {
	log.Printf("type: %v", data.My_type)
	switch data.My_type {
	case ROTATE:
		c := CommandRotate{}
		if err := json.Unmarshal(data.Content, &c); err != nil {
			log.Fatalf("err on rotate %v", err)
			break
		}
		return c, nil
	case QUERYINFO:
		return CommandQueryInfo{}, nil
	case CAMERAOPEN:
		return CommandOpenCamera{}, nil
	case CAMERACLOSE:
		return CommandCloseCamera{}, nil
	case RESET:
		return CommandReset{}, nil
	case BASEACTION:
		return CommandBaseAction{}, nil
	default:
		return nil, errors.New("No this type")
	}
	return nil, nil
}

// usual response command
type UsualResponse struct {
	Result  string `json:result`
	Content string `json:result`
}

func GenerateResponse(retval string) []byte { // ok , fail
	res := UsualResponse{Result: retval}
	body, err := json.Marshal(res)
	if err != nil {
		return nil
	}
	return body
}

// Base action
type CommandBaseAction struct{}

func (c CommandBaseAction) DealWith(conn net.Conn, wxsCmdJson *WxsCmdJson) error {
	log.Println("deal with base action command")
	conn.Write(GenerateResponse("OK"))
	return nil
}

// Reset command
type CommandReset struct{}

func (c CommandReset) DealWith(conn net.Conn, wxsCmdJson *WxsCmdJson) error {
	log.Println("deal with reset position command")
	conn.Write(GenerateResponse("OK"))
	return nil
}

// Close camera command
type CommandCloseCamera struct{}

func (c CommandCloseCamera) DealWith(conn net.Conn, wxsCmdJson *WxsCmdJson) error {
	log.Println("deal with close camera")
	conn.Write(GenerateResponse("FAIL"))
	return nil
}

// Open camera command
type CommandOpenCamera struct{}

func (c CommandOpenCamera) DealWith(conn net.Conn, wxsCmdJson *WxsCmdJson) error {
	log.Println("deal with open camera")
	conn.Write(GenerateResponse("FAIL"))
	return nil
}

// Query Info Command
type CommandQueryInfo struct{}
type CommandQueryInfoResContentDanceList struct {
	Id     string `json:id`
	Title  string `json:title`
	Cmd    string `json:command`
	Status string `json:status`
}
type CommandQueryInfoResContent struct {
	Name       string `json:name`
	Dance_list []CommandQueryInfoResContentDanceList
}
type CommandQueryInfoRes struct {
	Result  string `json:result`
	Content CommandQueryInfoResContent
}

func (c CommandQueryInfo) DealWith(conn net.Conn, wxsCmdJson *WxsCmdJson) error {
	log.Println("deal with query info")

	cqircd := []CommandQueryInfoResContentDanceList{
		{Id: "1111111", Title: "foo", Cmd: "ant"},
		{Id: "2222222", Title: "bar", Cmd: "boo"},
		{Id: "3333333", Title: "cat", Cmd: "cat"},
	}
	cqirc := CommandQueryInfoResContent{}
	cqirc.Name = "robot foo"
	cqirc.Dance_list = cqircd

	cqir := CommandQueryInfoRes{}
	cqir.Result = ""
	cqir.Content = cqirc

	body, err := json.Marshal(cqir)
	if err != nil {
		return err
	}

	conn.Write(body)
	return nil
}

// Rotate Command
type Args struct {
	T      int //  type
	Angles map[int]byte
	Speed  int
}
type CommandRotateData struct {
}
type CommandRotate struct {
	Id    int `json:id`
	Angle int `json:angle`
	Speed int `json:speed`
	Time  int `json:time`
}

func (c CommandRotate) DealWith(conn net.Conn, wxsCmdJson *WxsCmdJson) error {
	log.Println("deal with rotate command")

	data := &CommandRotateData{}
	err := json.Unmarshal([]byte(wxsCmdJson.Content), data)
	if err != nil {
		log.Fatalf("un json err: %v", err)
		return nil
	}

	log.Printf("CommandRotate-> id: %v angle: %v speed: %v time: %v ",
		c.Id, c.Angle, c.Speed, c.Time)
	if !SIMULATE {
		client, err := rpc.DialHTTP("tcp", ipaddress+":7999")
		defer client.Close()
		if err != nil {
			log.Fatal("Dialing:", err)
		}

		angles := make(map[int]byte)
		angles[c.Id] = byte(c.Angle)

		args := &Args{T: 1, Angles: angles, Speed: c.Speed}
		var retval int = -1
		err = client.Call("RobotRPC.Cmd", args, &retval)
		if err != nil {
			log.Fatal("rpc error: ", err)
		}
	}
	conn.Write(GenerateResponse("OK"))
	return nil
}

/* --------------------------- end of commands ------------------------------- */
