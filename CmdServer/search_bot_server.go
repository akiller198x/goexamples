package CmdServer

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os/exec"
)

type AddMeResponseDataContent struct {
	Ip string `json:"ip"`
}

type AddMeResponseData struct {
	Version string                   `json:"version"`
	Src_id  string                   `json:"src_id"`
	Dst_id  string                   `json:"dst_id"`
	My_type string                   `json:"type"`
	Content AddMeResponseDataContent `json:"content"`
}

type SearchServer struct{}

func GetLocalIPAddress() string {
	//cmd := exec.Command("ifconfig apcli0 | grep 'inet addr' | awk '{print $2}' |awk -F \":\" '{print $2}'")
	in := bytes.NewBuffer(nil)
	cmd := exec.Command("sh")
	cmd.Stdin = in
	go func() {
		in.WriteString("echo `ifconfig apcli0 | grep 'inet addr' | awk '{print $2}' |awk -F \":\" '{print $2}'` > ip.txt\n")
	}()

	if err := cmd.Run(); err != nil {
		return ""
	}

	return ""

	/*
			// GetLocalIP returns the non loopback local IP of the host
			addrs, err := net.InterfaceAddrs()
			if err != nil {
				return ""
			}
			for _, address := range addrs {
				// check the address type and if it is not a loopback the display it
				if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
					if ipnet.IP.To4() != nil {
						return ipnet.IP.String()
					}
				}
			}
		return ""
	*/
	/*
		ifaces, err := net.Interfaces()
		// handle err
		for _, i := range ifaces {
			addrs, err := i.Addrs()
			// handle err
			for _, addr := range addrs {
				var ip net.IP
				switch v := addr.(type) {
				case *net.IPNet:
					ip = v.IP
				case *net.IPAddr:
					ip = v.IP
				}
				// process IP address
			}
		}
	*/
}

func (s *SearchServer) Listen() {
	addr, err := net.ResolveUDPAddr("udp", ":9090")
	if err != nil {
		fmt.Println("err ", err)
		return
	}

	listener, err := net.ListenUDP("udp", addr)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("Local: <%s>\n", listener.LocalAddr().String())

	defer listener.Close()

	for {
		data := make([]byte, 256)
		n, remoteAddr, err := listener.ReadFromUDP(data)

		if err != nil {
			fmt.Println("error read data", err)
			continue
		}

		//fmt.Printf("<%s> %s\n", remoteAddr, data[:n])
		go forward(remoteAddr.IP.String(), string(data[:n]))
	}
}

func forward(ip, data string) {
	fmt.Printf("forward ->[%s]\n", data)

	var sendData AddMeResponseData
	sendData.Version = "0.1"
	sendData.Src_id = "1111111111111111111111"
	sendData.Dst_id = "1111111111111111111111"
	sendData.My_type = "ack_search_robot"

	amrc := AddMeResponseDataContent{Ip: GetLocalIPAddress()}
	//amrc := AddMeResponseDataContent{Ip: "192.168.2.119"}
	sendData.Content = amrc

	var port int = 9091
	ipaddr := fmt.Sprintf("%s:%d", ip, port)
	fmt.Printf("%s", ipaddr)
	fmt.Println()

	conn, err := net.Dial("tcp", ipaddr)
	defer conn.Close()
	if err != nil {
		log.Fatal(err)
	}

	body, err := json.Marshal(sendData)
	if err != nil {
		log.Fatal(err)
	}
	conn.Write(body)

	var msg [256]byte
	conn.Read(msg[:])
	log.Println("recv : ", string(msg[:]))
}
