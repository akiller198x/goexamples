package main

import (
	"fmt"
	"net"
)

func main() {
	fmt.Printf("Server is ready...\n")
	l, err := net.Listen("tcp", ":8053")
	if err != nil {
		fmt.Printf("Failure to listen: %s\n", err.Error())
	}

	for {
		if c, err := l.Accept(); err == nil {
			go Echo(c)
		}
	}
}
