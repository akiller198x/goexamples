package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/eduardonunesp/goaiml"
)

func ListDir(dirPath, suffix string) (files []string, err error) {
	files = make([]string, 0, 10)
	dir, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	pathSep := string(os.PathSeparator)
	suffix = strings.ToUpper(suffix)
	for _, fi := range dir {
		if fi.IsDir() {
			continue
		}
		if strings.HasSuffix(strings.ToUpper(fi.Name()), suffix) {
			files = append(files, dirPath+pathSep+fi.Name())
		}
	}
	return files, nil
}

func main() {
	/*
		files, err := ListDir("aiml", "aiml")
		if err != nil {
			panic(err)
		}
	*/

	aiml := goaiml.NewAIML()
	//err := aiml.Learn("test.aiml.xml")
	//if err != nil {
	//	log.Fatal(err)
	//}

	err := aiml.Learn("aiml/Default.aiml")
	if err != nil {
		log.Fatal(err)
	}
	/*
		for _, f := range files {
			fmt.Printf("%v", f)
			fmt.Println()

			err = aiml.Learn(f)
			if err != nil {
				log.Fatal(err)
			}

		}
	*/

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("You: ")
	for scanner.Scan() {
		line := scanner.Text()
		resp, _ := aiml.Respond(line)
		fmt.Println("Robot: " + resp)
		fmt.Print("You: ")
	}

}
